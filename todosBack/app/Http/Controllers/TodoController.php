<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use App\Http\Requests\StoreTodoRequest;
use App\Http\Requests\UpdateTodoRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

// use Yajra\DataTables\Facades\DataTables;

class TodoController extends Controller
{
    public function index(DataTables $datatables): JsonResponse
    {
        // $todos = Todo::all();
        // return response()->json(array('todos' => $todos), 200);

        $query = Todo::select('id', 'title', 'description', 'done');
        return $datatables->eloquent($query)->toJson();
    }

    public function store(StoreTodoRequest $request): JsonResponse
    {
        $todo = Todo::create($request->validated());
        return response()->json($todo, 201);
    }
    public function show(int $id): JsonResponse
    {
        $todo = Todo::find($id);
        if (!$todo) {
            return response()->json(['message' => 'Todo not found'], 404);
        }
        return response()->json(($todo), 200);
    }

    public function update(UpdateTodoRequest $request, int $id): JsonResponse
    {
        $todo = Todo::find($id);
        if (!$todo) {
            return response()->json(['message' => 'Todo not found'], 404);
        }
        $todo->update($request->validated());
        return response()->json(($todo), 200);
    }
    public function destroy(int $id): JsonResponse
    {
        $todo = Todo::find($id);
        if (!$todo) {
            return response()->json(['message' => 'Todo not found'], 404);
        }
        $todo->delete();
        return response()->json(null, 204);
    }
}
