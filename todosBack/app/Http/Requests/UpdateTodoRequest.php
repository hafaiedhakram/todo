<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateTodoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'done' => ['required', 'boolean'],
        ];
    }
    public function messages(): array
    {
        return [
            'title.required' => 'Titre est obligatoire',
            'description.required' => 'Description est obligatoire',
            'done.required' => 'Etat est obligatoire',
        ];
    }
    public function attributes(): array
    {
        return [
            'title' => 'Titre',
            'description' => 'Description',
            'done' => 'Etat',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
